
/*
const button = document.querySelector('.button');

button.addEventListener('click', async () => {

    const apiKey = '1a2fa5f457412c38709832e94db8093d';
    const input = document.querySelector('.search');
    const apiUrl = 'https://api.openweathermap.org/geo/1.0/direct';
    const apiUrl_2 = 'https://api.openweathermap.org/data/2.5/weather';
    const apiUrl_3 = 'https://api.openweathermap.org/data/2.5/forecast';

    const query = input.value;
    const response = await fetch(`${apiUrl}?q=${query}&limit=10&appid=${apiKey}`);
    const data = await response.json();
    document.querySelector('.selected_city').textContent = data[0].name;
    document.querySelector('.selected_city_2').textContent = data[0].name;

    const response_2 = await fetch(`${apiUrl_2}?q=${query}&appid=${apiKey}`);
    const data_2 = await response_2.json();
    document.querySelector('.degrees_feels').textContent = Math.round(data_2.main.feels_like - 273.15) + '°C';
    document.querySelector('.degrees_today').textContent = Math.round(data_2.main.temp - 273.15) + '°C';
    document.querySelector('.cloudiness').textContent = data_2.weather[0].main;
    document.querySelector('.weather_img_today').innerHTML = `<img src="https://openweathermap.org/img/wn/${data_2.weather[0]['icon']}@2x.png">`;

    const response_3 = await fetch(`${apiUrl_3}?q=${query}&appid=${apiKey}`);
    const data_3 = await response_3.json();
    const days = ['thu', 'fri', 'sat', 'sun', 'mon'];
    days.forEach((day, index) => {
        const weatherData = data_3.list[index * 8];
        const imgElement = `<img src="https://openweathermap.org/img/wn/${weatherData.weather[0]['icon']}@2x.png">`;
        document.querySelector(`.img_${day}`).innerHTML = imgElement;
        document.querySelector(`.cloud_description_${day}`).textContent = weatherData.weather[0].description;
        document.querySelector(`.${day}_weather_day`).textContent = `${Math.round(weatherData.main.temp_min - 273.15)}°C`;
        document.querySelector(`.${day}_weather_night`).textContent = `${Math.round(weatherData.main.temp_max - 273.15)}°C`;
        const str = weatherData.dt_txt;
        const arr = str.split("");
        document.querySelector(`.${day}`).textContent = arr[8] + arr[9];
    });
});
*/

const apiKey = '1a2fa5f457412c38709832e94db8093d';
const input = document.querySelector('.search');
const apiUrl = 'https://api.openweathermap.org/geo/1.0/direct';
const apiUrl_2 = 'https://api.openweathermap.org/data/2.5/weather';
const apiUrl_3 = 'https://api.openweathermap.org/data/2.5/forecast';

input.addEventListener('input', async () => {
    const query = input.value;
    const response = await fetch(`${apiUrl}?q=${query}&limit=10&appid=${apiKey}`);
    const data = await response.json();
    //   console.log(data);
    document.querySelector('.selected_city').textContent = data[0].name;
    document.querySelector('.selected_city_2').textContent = data[0].name;
});

input.addEventListener('input', async () => {
    const query = input.value;
    const response = await fetch(`${apiUrl_2}?q=${query}&appid=${apiKey}`);
    const data_2 = await response.json();
    //  console.log(data_2);
    document.querySelector('.degrees_feels').textContent = Math.round(data_2.main.feels_like - 273.15) + '°C';
    document.querySelector('.degrees_today').textContent = Math.round(data_2.main.temp - 273.15) + '°C';
    document.querySelector('.cloudiness').textContent = data_2.weather[0].main;
    document.querySelector('.weather_img_today').innerHTML = `<img src="https://openweathermap.org/img/wn/${data_2.weather[0]['icon']}@2x.png">`;
});

input.addEventListener('input', async () => {
    const query = input.value;
    const response = await fetch(`${apiUrl_3}?q=${query}&appid=${apiKey}`);
    const data_3 = await response.json();
  //  console.log(data_3);
    const days = ['thu', 'fri', 'sat', 'sun', 'mon'];
    days.forEach((day, index) => {
        const weatherData = data_3.list[index * 8];
        const imgElement = `<img src="https://openweathermap.org/img/wn/${weatherData.weather[0]['icon']}@2x.png">`;
        document.querySelector(`.img_${day}`).innerHTML = imgElement;
        document.querySelector(`.cloud_description_${day}`).textContent = weatherData.weather[0].description;
        document.querySelector(`.${day}_weather_day`).textContent = `${Math.round(weatherData.main.temp_min - 273.15)}°C`;
        document.querySelector(`.${day}_weather_night`).textContent = `${Math.round(weatherData.main.temp_max - 273.15)}°C`;
        const str = weatherData.dt_txt;
        const arr = str.split("");
        document.querySelector(`.${day}`).textContent = arr[8] + arr[9];
    });
});


















